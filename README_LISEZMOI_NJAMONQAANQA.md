# Nool language - A head-marking Verb-initial conlang

----usual boring disclaimer----

This paper, including its source code, is licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (BY-NC-SA). 

The licensor permits others to copy, distribute, display, and perform the work. In return, licenses may not use the work for commercial purposes, unless they get the licensor's permission. https://creativecommons.org/licenses/by-nc-sa/4.0/


tl;dr: please avoid commercial use without asking me: hanholDOThoguemATgmailDOTcom

Of course, as a language, you are free to re-use some morphosyntactic/-phonological features from it for your conlang.

---------------------------------------------------------------

If you compile this code on Overleaf, selecting XeLaTeX is advised. The PDF file is also avaiable on Google Drive at the following URL: https://drive.google.com/file/d/15554YtJZQEfRFEq-NwPN9B8NMcA-u_zZ/view?usp=sharing

---------------------------------------------------------------

English is not my native language, so mistakes and awkward sentences are highly likely to persist throughout this paper despite reviews.

---------------------------------------------------------------

This project is still under growth -one will say in beta-, therefore some features can still evolve deeply, or even be removed. As it is said in Conlanging niche: if a conlang is finished, then it is dead.

----/usual boring disclaimer----
```

git remote add origin https://gitlab.com/n3562/nool-language.git

```
Nool - _Nool_ (IPA: /no:l/ [n̻oːl(˦)˨˥])- is supposed to be spoken in an universe (or pluriverse if you prefer) roughly similar to ours, in which it belongs to the small Nolic family. Although distinct from Indo-European and Afro-Asiatic languages, interactions with both of them was numerous throughout history, whence many loanwords, especially from Spanish and Classical Arabic, and, more anciently, probably from Guanche...and, nowadays, from English of course. All these bunches of loanwords appear in a very Noolized form though, since Nool phonotax is rather strict: only CV(C)(C) syllables are allowed, with many restrictions in coda itself. For instance, only sonorants and voiceless fricatives or plosives can occur in it, and a complex coda (i.e. with more than one consonant) cannot co-occur with a long vowel in a syllable, since a syllable cannot weigh more than three morae -note that Nool is mora-timed-, two if the onset is ejective.

Nool is a highly agglutinative language with some fusional features. Hence the notion of stem is as accurate as that of root, but the very majority of the inflection and derivation is conveyed through affixal and clitic concatenation, whose patterns are neat enough to be mappable in "slots". Most roots are monosyllabic and CV(C), some are disyllabic, usually CVCV with the second syllable short (with many exceptions in loanwords).

As stems taking affixes, all roots are regular except _naj_ "talking, speech, saying, telling” and _baj_ "happening, event, process", to which one could add the kinship ones due to their alterations at their "determinee" forms.

The word order is Verb-initial, more commonly VSO, although VOS is rather frequent too. Like most VO-order languages, Nool is heavily head-initial/right-branched with a strict determinee-determinant order. It is a head-marking language, with an extensive -often mandatory- agreement of the head with its dependencies. This along with the possibility for a sentence to consist of a sole predicate. Nool has a rich set of conjunctive and discourse clitics (for topicalization, focalization, polarity...).

Other noticeable features are: a large amount of valency affixes, an alignement considered ergative or fluid-S, whose willing patients are encoded as agents (or even "austronesian", since adjuncts can be promoted as agents), noun-incorporation, "determinee" forms for some noun roots (often denoting constitutive parts) and the subordinate morphology which deeply diverges from the one of independent clause, and used in them -mandatorily with inflected negation and yes/no interrogation-. There is a blur between lexical categories: pronouns are hyperonymic nouns -and forming an open class- and any verb can be added an agent without any morphological derivation (see herebelow). Along with this, Nool is a zero-copula language.

As previously said, Nool is a heavily verb-based language, hence any unit of meaning is virtually usable as a verb, which is ergative, i.e. transitive or intransitive by omitting or adding the agent without any derivation required (English does have some of such verbs, yet as a closed class, e.g. break: "the owl [+agent] broke the branch [+patient]" - "the branch [+patient] broke"). The verb inflection marks person (more exactly polypersonalism), valency, volition, TAM and evidentiality, this in addition to noun -usually patient (or part thereof)- incorporation, so Nool verbs are prone to be rather long. Note that the so-called "valency" inflection can refer to any kind of adjunct -up to five per verb in theory-, including spatial ones, especially with positional verbs, and Nool has very few adpositions, many being relational nouns from body part ones. Identifying the verb is easy since it overall begins the sentence -only a focalized argument can front it- and has polypersonalism and/or willingness. It is worth noting that due to incorporation, simple sentences rarely display more than three unbound nominal arguments/adjuncts in spoken Nool

Conversely, nouns are morphosyntactically very simple, only inflected for possession -by means of some valency affixes and with alienability distinction-, and lacking gender, number (inferrable by polypersonal agreement, verb pluriactionality and context), definiteness and case inflections.


***************************

# La langue nôle - Une langue à marquage concentrique et à verbe en initiale

Le nôle - _Nool_ (API : /no:l/ [n̻oːl(˦)˨˥])- est censé se parler au sein d'un univers (ou plutôt plurivers) pas mal semblable au nôtre, dans lequel il appartient à la famille nôlique. Bien que n'appartenant ni aux langues indo-européennes ni aux langues afro-asiatiques, les interactions avec elles ont été récurrentes tout au long de l'histoire, d'où beaucoup d'emprunts, en particulier de l'espagnol et de l'arabe littéraire, et, plus anciennement, vraisemblablement du guanche...ainsi que, de nos jours, de l'anglais forcément. Cette multitude de sources d'emprunts se reflète dans des mots fort "nôlisés" toutefois, vu la rigidité de la phonotaxe :  seules les syllabes CV(C)(C) sont possibles, avec des restrictions au niveau des codas elles-mêmes -n'y apparaissent que les sonorantes et les fricatives et plosives sourdes, tandis qu'une coda complexe (càd avec plus d'une consonne) ne peut pas suivre une voyelle longue dans une syllabe, les syllabes ne pouvant peser plus de trois mores -à noter que le nôle a un rythme morique-, deux si l'attaque est éjective.

Le nôle est fortement agglutinant, avec quelques traits fusionnels. De fait, aussi bien les notions de radical que de racine sont adéquates, mais la grande majorité de la flexion et de la dérivation sont formés par concaténation, dont les schémas sont assez nets pour être décortiqués et démarqués. Bien des racines sont des monosyllabes de type CV(C), quelques-unes sont des disyllabes, généralement de type CVCV avec une seconde syllabe brève (avec de nombreuses exceptions dans les emprunts).

En tant que radicaux prenant des affixes, toutes les racines sont régulières hormis _naj_ "parole, dires, discussion” et _baj_ "évènement, processus, procès", auxquelles l'on pourrait ajouter celles de "filiation" vu leurs altérations à la forme "déterminée".

L'ordre des mots met le verbe en initiale, le plus souvent VSO, même si le VOS est courant lui aussi. Comme la plupart des langues plaçant le verbe en premier, le nôle est résolument centrifuge avec un ordre strictement déterminé-déterminant. C'est une langue concentrique, marquant systématiquement sur la tête du syntagme son accord -souvent obligatoirement- avec ses dépendances. Ce en parallèle avec la possibilité pour une phrase de se limiter à son prédicat. Le nôle a un inventaire fourni de clitiques conjonctifs et discursifs (pour la thématisation, la focalisation, la polarité, etc).

Il y a d'autres caractéristiques notables comme : un grand nombre d'affixes de valence, une structure d'actance considérée comme ergative ou active fluide, dont les patients délibérés sont encodés comme agents (voire "austronésienne", car les circonstants peuvent être promus comme agents), l'incorporation nominale, des formes "déterminées" avec quelques racines nominales (qui dénotent la plupart du temps des parties constitutives) ainsi que la morphologie des subordonnées, qui diverge en profondeur de celle des phrases indépendantes, et parfois utilisée avec celles-ci -obligatoirement en cas de négation flexionnelle et d'interrogation ouverte-. Les frontières entre catégories lexicales sont poreuses : les pronoms sont des noms hyperonymiques -qui forment une classe ouverte- et tout verbe peut se voir ajouter un agent sans qu'il faille passer par de la dérivation (voir ci-dessous). Ce d'autant plus que le nôle est une langue sans copule.

Comme déjà mentionné, le nôle a une morphosyntaxe qui se concentre autour du verbe, au point que toute unité de sens peut y être utilisé comme verbe, lequel est ergatif, càd qu'il est indifféremment transitif ou intransitif, en mentionnant ou non l'agent sans aucune dérivation marquée (cela se rencontre aussi avec quelques verbes en français, mais il s'y agit d'une classe fermée, par ex. casser : "la chouette [+agent] a cassé la branche [+patient]" - "la branche [+patient] a cassé"). La flexion verbale marque la personne (plus exactement la polypersonnalité), la valence, la volition, les TAM et l'évidentialité, ce en plus de l'incorporation nominale -généralement du patient (ou d'une partie de celui-ci)- par conséquent le nôle a tendance à avoir des verbes assez longs. Il est à noter que la flexion dite "de valence" peut aussi régir tout type de circonstant -en théorie jusqu'à cinq par verbe-, y compris spatiaux, surtout avec les verbes positionnels, le nôle a peu d'adpositions, beaucoup étant des noms relationnels eux-mêmes issus d'autres désignant des parties du corps. L'identification du verbe est facile étant donné qu'il débute en général la phrase -seul un argument focalisé peut se placer devant lui- et marque la polypersonnalité et/ou la volition. Il convient de noter que l'incorporation fait que les phrases comptent rarement plus de trois arguments/circonstants nominaux indépendants en nôle parlé.

Constrastivement, la morphosyntaxe du nom est très simple, se fléchissant seulement pour marquer la possession -au moyen de quelques affixes de valence en distingant l'aliénabilité-, mais ni pour le genre, le nombre (inférable par l'accord polypersonnel, la pluriactionnalité verbale et le contexte), la définitude ou le cas.



